/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_alloc_row.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:00:58 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 16:50:10 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

uint16_t	*ft_pgm_alloc_row(t_pgm data)
{
	return ((uint16_t *)malloc(sizeof(uint16_t) * data.width));
}
