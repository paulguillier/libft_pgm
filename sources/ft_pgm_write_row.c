/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_write_row.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 11:17:07 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 22:01:51 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

static int		ft_pgm_write_row_plain(t_pgm data, uint16_t *row)
{
	int	i;

	i = 0;
	while (i < data.width)
	{
		if (row[i] > data.maxval)
			return (-1);
		if (ft_putnbr_fd(row[i], data.file) < 0)
			return (-1);
		if (write(data.file, "\n", 1) < 0)
			return (-1);
		i++;
	}
	return (0);
}

static uint8_t	*ft_pgm_write_row_raw_one(uint16_t *row, int width)
{
	uint8_t	*buf;
	int		i;

	if ((buf = malloc(width)))
	{
		i = 0;
		while (i < width)
		{
			buf[i] = row[i];
			i++;
		}
	}
	return (buf);
}

static uint8_t	*ft_pgm_write_row_raw_two(uint16_t *row, int width)
{
	uint8_t	*buf;
	int		i;

	if ((buf = malloc(width * 2)))
	{
		i = 0;
		while (i < width)
		{
			ft_memcpy(buf + 2 * i, (void *)(row + i) + 1, 1);
			ft_memcpy(buf + 2 * i + 1, row + i, 1);
			i++;
		}
	}
	return (buf);
}

static int		ft_pgm_write_row_raw(t_pgm data, uint16_t *row)
{
	uint8_t	*buf;
	int		i;
	int		len;

	i = 0;
	if (data.bytes_per_sample == 1)
	{
		if ((buf = ft_pgm_write_row_raw_one(row, data.width)) == NULL)
			return (-1);
		len = data.width;
	}
	else
	{
		if ((buf = ft_pgm_write_row_raw_two(row, data.width)) == NULL)
			return (-1);
		len = data.width * 2;
	}
	if (write(data.file, buf, len) < 0)
	{
		free(buf);
		return (-1);
	}
	free(buf);
	return (0);
}

int				ft_pgm_write_row(t_pgm data, uint16_t *row)
{
	if (data.plain)
		return (ft_pgm_write_row_plain(data, row));
	return (ft_pgm_write_row_raw(data, row));
}
