/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_plainformat.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 18:32:55 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 16:52:29 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

int	ft_pgm_get_plainformat(t_pgm data, int *plainformat)
{
	if (plainformat)
		*plainformat = data.plain;
	return (data.plain);
}

int	ft_pgm_set_plainformat(t_pgm *data, int plainformat)
{
	if (data == NULL)
		return (-1);
	data->plain = !(!plainformat);
	return (data->plain);
}
