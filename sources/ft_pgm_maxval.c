/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_maxval.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 14:22:33 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 19:04:54 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

int	ft_pgm_get_maxval(t_pgm data, uint16_t *maxval)
{
	if (maxval)
		*maxval = data.maxval;
	if (!data.maxval)
		return (-1);
	return (0);
}

int	ft_pgm_set_maxval(t_pgm *data, uint16_t maxval)
{
	if (data == NULL)
		return (-1);
	if (maxval)
	{
		data->maxval = maxval;
		data->bytes_per_sample = (maxval < 256 ? 1 : 2);
	}
	return (0);
}
