/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_read_quit.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 10:27:02 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 16:50:05 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

int		ft_pgm_read_quit(t_pgm *data)
{
	int	ret;

	if (data == NULL)
		return (-1);
	ret = close(data->file);
	data->file = -1;
	data->width = 0;
	data->height = 0;
	data->plain = 0;
	return (ret);
}
