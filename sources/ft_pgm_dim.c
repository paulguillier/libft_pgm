/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_dim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 15:58:35 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/06 16:51:54 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

int	ft_pgm_get_dim(t_pgm data, int *width, int *height)
{
	if (width)
		*width = data.width;
	if (height)
		*height = data.height;
	if (!data.width || !data.height)
		return (-1);
	return (0);
}

int	ft_pgm_set_dim(t_pgm *data, int width, int height)
{
	if (data == NULL)
		return (-1);
	if (width)
		data->width = width;
	if (height)
		data->height = height;
	return (0);
}
