/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pgm_read_row.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 11:21:03 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 21:58:15 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_pgm.h"

static int	ft_pgm_read_row_plain_sample(t_pgm data, uint16_t *row, char c)
{
	uint16_t	t;

	*row = 0;
	while (!ft_isspace(c))
	{
		if (!ft_isdigit(c))
			return (-1);
		t = *row;
		*row = t * 10 + c - '0';
		if (*row < t)
			return (0);
		if (read(data.file, &c, 1) == 0)
			return (-1);
	}
	if (*row > data.maxval)
		return (-1);
	return (0);
}

static int	ft_pgm_read_row_plain(t_pgm data, uint16_t *row)
{
	char	c;
	int		i;

	i = 0;
	while (i < data.width)
	{
		if (read(data.file, &c, 1) == 0)
			return (-1);
		while (ft_isspace(c))
			if (read(data.file, &c, 1) == 0)
				return (-1);
		if (ft_pgm_read_row_plain_sample(data, row + i, c) < 0)
			return (-1);
		i++;
	}
	return (0);
}

static int	ft_pgm_read_row_raw_one(t_pgm data, uint16_t *row)
{
	int	i;

	i = 0;
	while (i < data.width)
	{
		if (read(data.file, row + i, 1) == 0)
			return (-1);
		if (row[i] > data.maxval)
			return (-1);
		i++;
	}
	return (0);
}

static int	ft_pgm_read_row_raw_two(t_pgm data, uint16_t *row)
{
	int	i;

	i = 0;
	while (i < data.width)
	{
		if (read(data.file, (uint8_t *)(row + i) + 1, 1) == 0)
			return (-1);
		if (read(data.file, (uint8_t *)(row + i), 1) == 0)
			return (-1);
		if (row[i] > data.maxval)
			return (-1);
		i++;
	}
	return (0);
}

int			ft_pgm_read_row(t_pgm data, uint16_t *row)
{
	if (data.file < 0 || row == NULL)
		return (-1);
	if (data.plain)
		return (ft_pgm_read_row_plain(data, row));
	if (data.bytes_per_sample == 1)
		return (ft_pgm_read_row_raw_one(data, row));
	return (ft_pgm_read_row_raw_two(data, row));
}
