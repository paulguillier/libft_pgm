/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_pgm.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 11:18:36 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 15:47:26 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PGM_H
# define LIBFT_PGM_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <stdint.h>

# include "types/t_pgm.h"
# include "libft/libft.h"

int			ft_pgm_read_init(char *file, t_pgm *data);
int			ft_pgm_read_row(t_pgm data, uint16_t *row);
int			ft_pgm_read_quit(t_pgm *data);

int			ft_pgm_write_init(char *file, t_pgm *data);
int			ft_pgm_write_row(t_pgm data, uint16_t *row);
int			ft_pgm_write_quit(t_pgm *data);

uint16_t	*ft_pgm_alloc_row(t_pgm data);
void		ft_pgm_free_row(uint16_t *row);

int			ft_pgm_get_dim(t_pgm data, int *width, int *height);
int			ft_pgm_set_dim(t_pgm *data, int width, int height);

int			ft_pgm_get_maxval(t_pgm data, uint16_t *maxval);
int			ft_pgm_set_maxval(t_pgm *data, uint16_t maxval);

int			ft_pgm_get_plainformat(t_pgm data, int *plainformat);
int			ft_pgm_set_plainformat(t_pgm *data, int plainformat);

#endif
