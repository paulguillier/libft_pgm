/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_pgm.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguillie <pguillie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 19:55:24 by pguillie          #+#    #+#             */
/*   Updated: 2018/11/07 19:04:54 by pguillie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_PGM_H
# define T_PGM_H

struct	s_pgm
{
	int			file;
	int			width;
	int			height;
	int			plain;
	uint16_t	maxval;
	int			bytes_per_sample;
};

typedef struct s_pgm	t_pgm;

#endif
